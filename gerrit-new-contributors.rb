require 'cgi'
require 'date'
require 'json'
require 'net/http'
require 'pp'
require 'uri'
require 'yaml'

Gerrit_Patch_Uploader = 1150 # Gerrit Patch Uploader's account ID

def query path
	resp = Net::HTTP.get URI.parse("https://gerrit.wikimedia.org/r/#{path}")
	data = JSON.parse resp.sub(/^\)\]\}\'/, '')
	data
end

if ARGV.length != 2
	today = Date.today
	end_date = Date.new(today.year, today.month, 1)
	start_date = end_date.prev_month
	puts "Find new contributors who appeared between the given dates"
	puts "usage: #{$0} <start-date> <end-date>"
	puts "  e.g. #{$0} #{start_date} #{end_date}"
	exit
end

start_date = Date.parse ARGV[0]
end_date = Date.parse ARGV[1]

puts "Figuring out repos"
urls = %w[
	https://codesearch.wmcloud.org/deployed/
	https://codesearch.wmcloud.org/libraries/
]
deployed_repos = urls.map{|u| Net::HTTP.get(URI.parse u)[/CS_JSDATA = (.+);/, 1] }
	.map{|json| JSON.parse(json)['reposData'].values.map{|a| a['url'] } }.inject(:+)
	.map{|u| u[ %r[^https://gerrit(?:-replica)?.wikimedia.org/r/(.+).git$], 1 ] }.compact.uniq
# no GitLab or GitHub support for now

# We have to make multiple smaller queries, otherwise Gerrit fails with "414 Request-URI Too Long"

puts "Searching for contributors between #{start_date} and #{end_date}"
contributions = deployed_repos.each_slice(50).map{|repos|
	repos_q = "(#{ repos.map{|r| "project:#{r}" }.join(' OR ') })"
	q = "#{repos_q} status:merged mergedafter:#{start_date} mergedbefore:#{end_date} -ownerin:ldap/wmf -ownerin:ldap/wmde -owner:#{Gerrit_Patch_Uploader}"
	data = query("changes/?q=#{CGI.escape q}")
	while !data.empty? && data.last['_more_changes']
		data += query("changes/?q=#{CGI.escape q}&start=#{data.length}")
	end
	data.map{|a| a['owner']['_account_id']}
}.inject(:+)

gpu_contributions = deployed_repos.each_slice(50).map{|repos|
	repos_q = "(#{ repos.map{|r| "project:#{r}" }.join(' OR ') })"
	q = "#{repos_q} status:merged mergedafter:#{start_date} mergedbefore:#{end_date} owner:#{Gerrit_Patch_Uploader}"
	data = query("changes/?q=#{CGI.escape q}&o=CURRENT_REVISION&o=CURRENT_COMMIT")
	while !data.empty? && data.last['_more_changes']
		data += query("changes/?q=#{CGI.escape q}&o=CURRENT_REVISION&o=CURRENT_COMMIT&start=#{data.length}")
	end
	data.map{|a| a['revisions'][ a['current_revision'] ]['commit']['author']['email'] }
}.inject(:+)

puts "#{contributions.length} patches by #{contributions.uniq.length} volunteer contributors"
puts "(plus #{gpu_contributions.length} patches by #{gpu_contributions.uniq.length} volunteer contributors via Gerrit Patch Uploader)"

puts "Checking for their earlier contributions..."

contributions.uniq.each do |id|
	account = query("accounts/#{id}")
	next if account['tags'] && account['tags'].include?('SERVICE_USER')
	older_patches = deployed_repos.each_slice(50).map{|repos|
		repos_q = "(#{ repos.map{|r| "project:#{r}" }.join(' OR ') })"
		q = "#{repos_q} status:merged mergedbefore:#{start_date} owner:#{id} limit:1"
		query("changes/?q=#{CGI.escape q}")
	}.inject(:+)
	next if older_patches.length != 0
	puts "New contributor: https://gerrit.wikimedia.org/r/q/owner:#{account['email']}"
end

gpu_contributions.uniq.each do |email|
	older_patches = deployed_repos.each_slice(50).map{|repos|
		repos_q = "(#{ repos.map{|r| "project:#{r}" }.join(' OR ') })"
		q = "#{repos_q} status:merged mergedbefore:#{start_date} author:#{email} limit:1"
		query("changes/?q=#{CGI.escape q}")
	}.inject(:+)
	next if older_patches.length != 0
	puts "New contributor: https://gerrit.wikimedia.org/r/q/author:#{email}"
end
